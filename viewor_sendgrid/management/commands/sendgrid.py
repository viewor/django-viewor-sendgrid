from django.core.management.base import BaseCommand, CommandError
from django.db import IntegrityError, transaction
from django.db import connection
# VIEWOR SENGRID
from viewor_sendgrid.models import EmailDestinatarioDefault, EmailBase, EmailBaseDestinatario
from django.conf import settings


def obtenerCorreoBase():
    if not settings.VIEWOR_SENDGRID_SUBJECT:
        raise("Need add in settings -> VIEWOR_SENDGRID_SUBJECT = 'Viewor'")
    if not settings.VIEWOR_SENDGRID_ASUNTO:
        raise("Need add in settings -> VIEWOR_SENDGRID_ASUNTO = 'Event detect'")
    if not settings.VIEWOR_SENDGRID_FROM:
        raise("Need add in settings -> VIEWOR_SENDGRID_FROM = 'My name <example@example.com>'")
    return {
            "clave":"sistema_eventos",
            "html":"correos/genericos/correo_mensajes.html",
            "subject":settings.VIEWOR_SENDGRID_SUBJECT,
            "asunto":settings.VIEWOR_SENDGRID_ASUNTO,
            "desde":settings.VIEWOR_SENDGRID_FROM
        }


def asigna_correos_base():
    defaults = EmailDestinatarioDefault.objects.filter(habilitado=True)
    bases = EmailBase.objects.all()
    for base in bases:
        [ EmailBaseDestinatario.objects.get_or_create(email_base=base, destinatario=x) for x in defaults]

def carga_conf():
    correos = [x[1] for x in settings.ADMINS]
    [ EmailDestinatarioDefault.objects.get_or_create(email=x) for x in correos ] # EmailDestinatarioDefault
    EmailBase.objects.get_or_create(**obtenerCorreoBase())
    asigna_correos_base()


class Command(BaseCommand):
    help = 'Carga la configuracion para el sistema de eventos de viewor sendgrid'

    def handle(self, *args, **options):
        try:
            print("INICIANDO TAREA")
            carga_conf()
            print("TAREA TERMINADA")
        except Exception as e:
            print("ERROR: %s" % e)