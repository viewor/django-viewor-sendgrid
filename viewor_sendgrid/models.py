# -*- coding: utf-8 -*-
from django.db import models


class EmailBase(models.Model):
    clave = models.CharField(max_length=40, unique=True)
    html = models.CharField(max_length=100)
    subject = models.CharField(max_length=100)
    asunto = models.CharField(max_length=100)
    desde = models.TextField(max_length=100)
    important = models.BooleanField(default=True)
    track_opens = models.BooleanField(default=True)
    track_clicks = models.BooleanField(default=True)
    auto_text = models.BooleanField(default=True)
    auto_html = models.BooleanField(default=False)
    inline_css = models.BooleanField(default=False)
    url_strip_qs = models.BooleanField(default=True)
    preserve_recipients = models.BooleanField(default=False)
    asincrono = models.BooleanField(default=False)
    tags = models.TextField(null=True, blank=True)
    send_at = models.DateTimeField(null=True, blank=True)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.clave

    def obten_lista_destinatarios(self):
        return list(self.emailbasedestinatario_set.
                    filter(destinatario__habilitado=True).values_list("destinatario__email", flat=True))


class EmailDestinatarioDefault(models.Model):
    email = models.EmailField(max_length=100, unique=True)
    habilitado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email


class EmailBaseDestinatario(models.Model):
    destinatario = models.ForeignKey(EmailDestinatarioDefault, on_delete=models.CASCADE)
    email_base = models.ForeignKey(EmailBase, on_delete=models.CASCADE)

    def __str__(self):
        return u"%s - %s" % (self.destinatario, self.email_base)


class LogEmail(models.Model):
    emailbase = models.ForeignKey(EmailBase, on_delete=models.CASCADE)
    destinatarios = models.TextField()
    de = models.CharField(max_length=100)
    enviado = models.BooleanField()
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"%s - %s" % (self.emailbase, "Enviado" if self.enviado else "No enviado")


class EmailTemplate(models.Model):
    id_template = models.CharField(max_length=100, unique=True)
    nombre_template = models.CharField(max_length=100, unique=True)
    asunto = models.CharField(max_length=100)
    desde = models.TextField(max_length=100)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.id_template

    def obten_lista_destinatarios(self):
        return list(self.emailtemplatebcc_set.
                    filter(destinatario__habilitado=True).values_list("destinatario__email", flat=True))


class BccTemplate(models.Model):
    email = models.EmailField(max_length=100, unique=True)
    habilitado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email

class EmailTemplateBcc(models.Model):
    destinatario = models.ForeignKey(BccTemplate, on_delete=models.CASCADE)
    email_template = models.ForeignKey(EmailTemplate, on_delete=models.CASCADE)

    def __str__(self):
        return u"%s - %s" % (self.destinatario, self.email_template)


class LogEmailTemplate(models.Model):
    email = models.ForeignKey(EmailTemplate, on_delete=models.CASCADE)
    destinatarios = models.TextField()
    enviado = models.BooleanField()
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"%s - %s" % (self.email, "Enviado" if self.enviado else "No enviado")