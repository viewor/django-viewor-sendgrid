# -*- coding: utf-8 -*-
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from ..models import LogEmail, EmailBase, EmailTemplate, LogEmailTemplate
from .core import VieworSendgrid

def email_render_html(template, datos):
    html_body = render_to_string(template, datos)
    return html_body


def instanciacorreo(emailbase, destinatarios, html):
    destinatarios_bcc = emailbase.obten_lista_destinatarios()
    if settings.DEBUG == False and settings.TEST_MODE == False:
        destinatarios = destinatarios
        if not destinatarios:
            destinatarios = destinatarios_bcc
            destinatarios_bcc = []
        else:
            destinatarios = list(set(destinatarios).difference(destinatarios_bcc))
    else:
        destinatarios = destinatarios_bcc
        destinatarios_bcc = []


    email = VieworSendgrid()
    email.from_email = emailbase.desde
    email.to_email = destinatarios
    email.subject = emailbase.subject
    email.content_type = "text/html"
    email.content = html
    email.bcc_email = destinatarios_bcc

    # Datos para el log
    log = LogEmail()
    log.emailbase = emailbase
    log.destinatarios = str(destinatarios)
    log.de = emailbase.desde
    log.enviado = False
    return email, log

# Base para envio de correos se pueden hacer mas bases tomando como ejemplo este
def email_base(clave_email, destinatarios=[], datos={}, de=None, html=None):
    email = EmailBase.objects.get(clave=clave_email)
    html = email_render_html(email.html, datos)
    emailobj, log = instanciacorreo(email, destinatarios, html)
    enviado, respuesta = emailobj.send_email()
    if enviado:
        log.enviado = True
    log.save()
    return log.enviado

class SendEmailTemplate(object):
    destinatarios = []
    destinatarios_bcc = []

    def __init__(self, nombre_template, destinatarios=[], datos={}):
        super(SendEmailTemplate, self).__init__()
        self.api = VieworSendgrid()
        self.email = self.getEmail(nombre_template)
        self.setDestinatarios(destinatarios)
        self.datos = datos


    def creaLog(self):
        log = LogEmailTemplate()
        log.email = self.email
        log.destinatarios = str(self.destinatarios_bcc + self.destinatarios)
        log.enviado = False
        log.save()
        return log


    def getEmail(self, nombre_template):
        return EmailTemplate.objects.get(nombre_template=nombre_template)

    def getApi(self):
        self.api.from_email = self.email.desde
        self.api.to_email = self.destinatarios
        self.api.subject = self.email.asunto
        self.api.content_type = "text/html"
        self.api.content = "<p>{nombre_template}</p>".format(nombre_template=self.email.nombre_template)
        self.api.bcc_email = self.destinatarios_bcc
        self.api.substitution = self.datos
        self.api.template_id = self.email.id_template
        return self.api

    def setApi(self, api):
        self.api = api

    def setDestinatarios(self, destinatarios):
        destinatarios_bcc = self.email.obten_lista_destinatarios()
        if settings.DEBUG == False and settings.TEST_MODE == False:
            destinatarios = destinatarios
            if not destinatarios:
                destinatarios = destinatarios_bcc
                destinatarios_bcc = []
            else:
                destinatarios = [destinatario for destinatario in set(destinatarios).difference(destinatarios_bcc)]
        else:
            destinatarios = destinatarios_bcc
            destinatarios_bcc = []
        self.destinatarios_bcc = destinatarios_bcc
        self.destinatarios = destinatarios

    def send_email(self):
        api = self.getApi()
        log = self.creaLog()
        enviado, respuesta = api.send_email()
        if enviado == True:
            log.enviado = False
            log.save()
        return enviado, respuesta

