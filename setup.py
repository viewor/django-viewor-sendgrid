# -*- coding: utf-8 -*-
import os
from setuptools import find_packages, setup

#with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
#    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-viewor-sendgrid',
    version='0.15',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',  # example license
    description='Un simple cliente para sendgrid.',
    #long_description=README,
    url='https://www.viewor.mx/',
    author='Viewor',
    install_requires=["sendgrid", "json2html"],
    author_email='enrique.wx@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.11',  # replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # Replace these appropriately if you are stuck on Python 2.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)