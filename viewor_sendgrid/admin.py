from django.contrib import admin
from .models import (EmailBase, LogEmail, EmailDestinatarioDefault, EmailBaseDestinatario,
                     EmailTemplate, BccTemplate, EmailTemplateBcc, LogEmailTemplate)


class EmailBaseDestinatarioInline(admin.TabularInline):
    model = EmailBaseDestinatario

class EmailBaseAdmin(admin.ModelAdmin):
    list_display = ('clave', "asunto", "html")
    search_fields = ['clave', "asunto", "html"]
    inlines = [
        EmailBaseDestinatarioInline,
    ]
admin.site.register(EmailBase, EmailBaseAdmin)


class EmailDestinatarioDefaultAdmin(admin.ModelAdmin):
    list_display = ('email', "habilitado", "creado", "actualizado")
    search_fields = ['email']
admin.site.register(EmailDestinatarioDefault, EmailDestinatarioDefaultAdmin)


class EmailBaseDestinatarioAdmin(admin.ModelAdmin):
    list_display = ('destinatario', "email_base")
    search_fields = ['destinatario__email', 'email_base__clave']
admin.site.register(EmailBaseDestinatario, EmailBaseDestinatarioAdmin)

class LogEmailAdmin(admin.ModelAdmin):
    list_display = ('emailbase', "destinatarios", "de", "enviado", "creado")
    search_fields = ['emailbase__clave', "destinatarios", "de"]
admin.site.register(LogEmail, LogEmailAdmin)



class EmailTemplateBccInline(admin.TabularInline):
    model = EmailTemplateBcc

class EmailTemplateAdmin(admin.ModelAdmin):
    list_display = ('id_template', "nombre_template", "asunto", "desde")
    search_fields = ['id_template', "nombre_template", "asunto"]
    inlines = [
        EmailTemplateBccInline,
    ]
admin.site.register(EmailTemplate, EmailTemplateAdmin)


class BccTemplateAdmin(admin.ModelAdmin):
    list_display = ('email', "habilitado", "creado", "actualizado")
    search_fields = ['email']
admin.site.register(BccTemplate, BccTemplateAdmin)


class EmailTemplateBccAdmin(admin.ModelAdmin):
    list_display = ('destinatario', "email_template")
    search_fields = ['destinatario__email', 'email_template__nombre_template']
admin.site.register(EmailTemplateBcc, EmailTemplateBccAdmin)


class LogEmailTemplateAdmin(admin.ModelAdmin):
    list_display = ('email', "destinatarios", "enviado", "creado")
    search_fields = ['email__clave', "destinatarios"]
admin.site.register(LogEmailTemplate, LogEmailTemplateAdmin)