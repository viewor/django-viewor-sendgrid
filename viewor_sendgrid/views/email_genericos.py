# -*- coding: utf-8 -*-
from .email_helpers import email_base
from django.utils import timezone
import traceback
from json2html import *


# Envio de excepciones
def sistema_eventos(destinatarios=[], datos={}, de=None, html=None):
    return email_base(clave_email="sistema_eventos", destinatarios=destinatarios, datos=datos, de=de, html=html)

def envia_excepcion(evento="Exception", funcion="", dictJson=None):
    from django.conf import settings
    datos = {}
    excepcion = traceback.format_exc()
    strJson = None
    if dictJson:
        strJson = json2html.convert(json = dictJson)
    datos["evento"] = evento
    datos["funcion"] = funcion
    datos["fecha"] = timezone.now().strftime("%A %d de %B del %Y a las %H:%M:%S")
    datos["excepcion"] = None if "NoneType\n" == excepcion else excepcion
    datos["strJson"] = strJson
    if settings.DEBUG == False:
        sistema_eventos(datos=datos)
    else:
        print(excepcion)