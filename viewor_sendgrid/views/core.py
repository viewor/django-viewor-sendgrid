import sendgrid
import os
from sendgrid.helpers.mail import *
from django.conf import settings


class VieworSendgrid(object):
	from_email = ""
	from_email_name = ""
	to_email = [] # Email Format [("enrique.wx@gmail.com", "nombre")]
	subject = ""
	content_type = ""
	content = ""
	cc_email = [] # Email Format [("enrique.wx@gmail.com", "nombre"),]
	bcc_email = [] # Email Format [("enrique.wx@gmail.com", "nombre"),]
	headers = [] # [("X-Test", "test"),]
	send_at = None
	substitution = {} # {"nombre": "saul", "fecha": "02/07/1999"}  El formato de la fecha es el que quieras
	template_id = None

	"""docstring for ClassName"""
	def __init__(self):
		super(VieworSendgrid, self).__init__()
		self.sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
		

	def getToEmail(self, lista):
		listaEmails = []
		for email in lista:
			if isinstance(email, str):
				listaEmails.append(Email(email))
			elif len(email) > 1:
				listaEmails.append(Email(email[0], email[1]))
			else:
				listaEmails.append(Email(email[0]))
		return listaEmails

	def getHeaders(self):
		return [Header(header[0], header[1]) for header in self.headers]

	def getSubstitution(self):
		''' La salida seria: [Substitution("%key%", valor), etc]'''
		return [Substitution("%{key}%".format(key=key), self.substitution[key]) for key in self.substitution.keys()]	


	def send_email(self):
		mail = Mail()
		mail.from_email = Email(self.from_email, self.from_email_name)
		mail.subject = self.subject
		mail.add_content(Content(self.content_type, self.content))
		mail.add_personalization(self.build_personalization())
		if self.template_id:
			mail.template_id = self.template_id
		try:
			response = self.sg.client.mail.send.post(request_body=mail.get())
			if response.status_code == 202:
				return True, response.body
			else:
				return False, response.body
		except Exception as e:
			return False, e


	def build_personalization(self):
		"""Build personalization mock instance from a mock dict"""
		personalization = Personalization()
		for to_addr in self.getToEmail(self.to_email):
			personalization.add_to(to_addr)

		for cc_addr in self.getToEmail(self.cc_email):
			personalization.add_to(cc_addr)

		for bcc_addr in self.getToEmail(self.bcc_email):
			personalization.add_bcc(bcc_addr)

		for header in self.getHeaders():
			personalization.add_header(header)

		for substitution in self.getSubstitution():
			personalization.add_substitution(substitution)

		personalization.subject = self.subject
		if self.send_at:
			personalization.send_at = self.send_at
		return personalization
