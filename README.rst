=====
 DJANGO VIEWOR SENDGRID
=====

** ¿Como Esta Funciona? **

    El sistema de correo de Viewor ha sido creado para dar soporte a todas las aplicaciones que esten en el mismo proyecto,
    el cual cuenta con herramientas para facilitar el envio de correos sin importar en que aplicacion se este.

    Para el envio de correos se usa sendgrid-django que permite enviar correos atravez de sendgrid sin tener que hacer modificaciones
    extras al servidor.

**¿Como configurar un correo para ser enviado?**

En su aplicacion, debe existir el archivo "correos.py" si no existe creelo, este archivo es el guia donde se registraran los correos para su aplicacion.

EJEMPLO:

    correos.py
    from General.correos.views.helpers import email_base

    def email_primer_email(destinatarios=[], datos={}, de=None, html=None):
        return email_base(clave_email="primer-email-appName", destinatarios=destinatarios, datos=datos, de=de, html=html)

Donde se tiene que importar 

    from General.correos.views.helpers import email_base

Despues tiene que agregar una funcion en el ejemplo de arriba se nombro "def email_primer_email(destinatarios=[], datos={}, de=None, html=None)" pero le puede nombrar como quiera. Los parametros de entrada tienen que ser siempre iguales asi como el contenido. Lo unico que tendra que cambiar es la clave_email la cual debe ser unica.

**TEMPLATES**

Despues debe crear el html del correo, recomiendo crear una estructura parecido a esto:

    appName
        -templates
            -appName
                -correos
                    base_email.html
                    miprimercorreo.html

Dentro de la carpeta crear el base del correo si es que asi se necesita o simplemente crear el html. Los html soportan casi todas las opciones de django templates ;)


**EMAIL BASE (ADMIN)**

Despues que tenga estos 2 debe dirigirse a su navegador y atravez del admin de Django podra configurar su correo.

Dirigase a la seccion llamada correos y seleccione "Email bases" despues de clic en "Agregar email base" y llene los datos como corresponde

    **DATOS**
        **Clave**: "Es la clave que asigno el archivo correos.py"
        **Html**: "La ruta donde quedo el correo a enviar por ejemplo appName/correos/miprimercorreo.html"
        **Subject**: "Aun sin determinar xD"
        **Asunto**: "El asunto del correo por ejempo, Este es mi primer correo"
        **Desde**: "Quien en envia el correo por ejemplo, contacto@appName.com"
        **Checkbox**, Tags, Send at: "solo ignorelos, eran para otra libreria"

    **EMAIL BASE DESTINATARIOS**
        Elija a quien se le va mandar por default este correo, se envia una copia oculta las personas aqui seleccionadas
        Si no existe el correo deseado, agregelo.

**ENVIAR CORREO DESDE EL LUGAR SELECCIONADO**

Ahora solo queda importar la funcion creada en correos.py en donde lo quieras enviar y usarlo en donde se quiera enviar ejemplo

    email_primer_email(
                destinatarios=["micorreo@miproveedor.com"],
                datos={
                        "mensaje": "HOLA MUNDO"
                       }
                )




